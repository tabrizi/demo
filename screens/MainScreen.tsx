/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  KeyboardAvoidingView,
  Keyboard,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {TextFiled} from '../components/textfiled';

export default function TabOneScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showCancelButton, setShowCancelButton] = useState(true);

  const next = () => {
    console.log('lets go!!');
  };

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      _keyboardDidShow,
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      _keyboardDidHide,
    );

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);

  const _keyboardDidShow = () => {
    setShowCancelButton(false);
  };

  const _keyboardDidHide = () => {
    setShowCancelButton(true);
  };

  return (
    <KeyboardAvoidingView style={[styles.container]}>
      <View
        style={[styles.inner, {maxHeight: showCancelButton ? '80%' : '100%'}]}>
        <ScrollView>
          <View>
            <View style={styles.wfullDiv}>
              <TextFiled
                placeholder=""
                multiline={false}
                onChange={(e: any) => {
                  setEmail(e);
                }}
                value={email}
                label="Email"
                maxChars={30}
                password={false}
              />
            </View>
            <View style={styles.wfullWithMargin}>
              <TextFiled
                placeholder=""
                multiline={false}
                onChange={(e: any) => {
                  setPassword(e);
                }}
                label="Password"
                value={password}
                maxChars={30}
                password
              />
            </View>
            <View style={styles.socialButtonWrapper}>
              <TouchableOpacity style={styles.socialButton}>
                <FontAwesome name="apple" size={24} />
                <Text style={styles.socialButtonText}>Sign up with Apple</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.socialButtonWrapper}>
              <TouchableOpacity style={styles.socialButton}>
                <FontAwesome name="facebook" color="blue" size={24} />
                <Text style={styles.socialButtonText}>
                  Sign up with Facebook
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.wfullDiv}>
              <Text style={styles.hintText}>
                By creating an account, you are indicating that you agree the{' '}
                <Text style={styles.termLinkText}>Terms of Use</Text> and you
                are over the age of 13.
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.nextButtonWrapper}>
          <TouchableOpacity
            disabled={!(email && password)}
            onPress={next}
            style={
              email && password ? styles.nextButton : styles.nextButtonDisabled
            }>
            <Text style={styles.nextButtonText}>Next</Text>
          </TouchableOpacity>
          {showCancelButton && (
            <TouchableOpacity style={styles.cancelButton}>
              <Text style={styles.cancelButtonText}>Cancel</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    justifyContent: 'flex-end',
  },
  inner: {
    padding: 24,
    paddingBottom: 8,
    flex: 1,
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  socialButtonText: {marginLeft: 12},
  socialButton: {
    borderColor: '#383838',
    borderWidth: 1,
    borderRadius: 50,
    padding: 8,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    maxWidth: '60%',
    minWidth: '60%',
  },
  socialButtonWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8,
  },
  hintText: {
    color: 'gray',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 48,
  },
  cancelButtonText: {
    color: 'gray',
    fontWeight: 'bold',
    fontSize: 16,
    textDecorationStyle: 'solid',
    textDecorationColor: 'gray',
    textDecorationLine: 'underline',
  },
  nextButtonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 18,
  },
  nextButtonDisabled: {
    backgroundColor: '#d4d4d4',
    borderRadius: 50,
    padding: 12,
    width: 200,
    alignItems: 'center',
    marginTop: 12,
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: {width: 1, height: 13},
  },
  nextButton: {
    backgroundColor: '#b31717',
    borderRadius: 50,
    padding: 12,
    width: 200,
    alignItems: 'center',
    marginTop: 12,
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: {width: 1, height: 13},
  },
  nextButtonWrapper: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
  },
  cancelButton: {
    borderRadius: 50,
    width: '64%',
    alignItems: 'center',
    marginTop: 24,
    marginBottom: 24,
  },
  termLinkText: {
    textDecorationStyle: 'solid',
    textDecorationColor: 'gray',
    textDecorationLine: 'underline',
  },
  textfildStyle: {
    borderBottomColor: 'lightgray',
    borderBottomWidth: 2,
  },
  wfullDiv: {
    width: '100%',
  },
  wfullWithMargin: {
    width: '100%',
    marginBottom: 24,
  },
});
