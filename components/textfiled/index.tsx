import React from 'react';
import {StyleSheet, View} from 'react-native';
import FloatingLabelInput from 'react-native-floating-labels';

interface Props {
  onChange: (e: any) => void;
  value: any;
  keyboardType: any;
  placeholder: string | null;
  label: string;
  multiline: boolean;
  maxChars: number | null;
  password: boolean;
}

function TextFiled({onChange, value, label, maxChars, password}: Props) {
  return (
    <View>
      <FloatingLabelInput
        value={value}
        onChangeText={(e: any) => {
          let t = e;

          if (maxChars) {
            t = t.toString().slice(0, maxChars);
            onChange(t);
          } else {
            onChange(t);
          }
        }}
        password={password}
        labelStyle={styles.labelInput}
        inputStyle={styles.input}
        style={styles.formInput}>
        {label}
      </FloatingLabelInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  labelInput: {
    color: 'gray',
  },
  formInput: {
    borderBottomWidth: 1.5,
    borderColor: '#ccc',
  },
  input: {
    borderWidth: 0,
    borderRadius: 0,
    overflow: 'hidden',
    padding: 0,
  },
});

TextFiled.defaultProps = {
  keyboardType: 'default',
};

export {TextFiled};
