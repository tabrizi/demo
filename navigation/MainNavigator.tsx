import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import MainScreen from '../screens/MainScreen';
import {TabOneParamList} from '../types';

const MainPageStack = createStackNavigator<TabOneParamList>();

export default function MainNavigator() {
  return (
    <MainPageStack.Navigator>
      <MainPageStack.Screen
        name="MainPageStack"
        component={MainScreen}
        options={{headerShown: false}}
      />
    </MainPageStack.Navigator>
  );
}
